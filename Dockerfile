FROM ubuntu:16.04

ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN apt-get update && apt-get install build-essential python3 python3-dev libssl-dev libffi-dev python3-pip -y --no-install-recommends apt-utils

RUN mkdir /srv/data
COPY requirements.txt /srv/requirements.txt

RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install -r /srv/requirements.txt

WORKDIR /srv
COPY config.py /srv/config.py
COPY extraconfig.py /srv/extraconfig.py
COPY plugins /srv/plugins

EXPOSE 3141 3142

CMD ["errbot"]
