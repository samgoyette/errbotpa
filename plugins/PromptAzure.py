from errbot import BotPlugin, botcmd, arg_botcmd, cmdfilter
import requests, os, json, yaml

class PromptAzure(BotPlugin):

	def post_webhook(self, uri, body):
		ret = ""
		try:
			r = requests.post(uri, json=body)
			if (r.status_code == 202) :
				ret = "Runbook was successfully queued."
			else :
				ret = "Error. Status code: " + r.status_code + " Text: " + r.text
		except Exception as e:
			ret = "Exception in POSTing to the webhook: " + str(e)
		return ret


	# Stop or start VMs
	# args:
	#	- VMName (string) : name of the VM to restart
	#	- Action (string) : Start or Stop
	#
	@botcmd
	@arg_botcmd('--vmname', dest="vmname", type=str)
	@arg_botcmd('--action', dest="action", type=str)
	def vmmanage(self, msg, vmname, action ):

		data_path = os.path.dirname(__file__) + "/PromptAzure.yaml"
		with open(data_path, 'r') as f:
			data = yaml.load(f)

		if (action.upper() != "START") and (action.upper() != "STOP"):
			yield "Error: Action must be either 'start' or 'stop'"
			return

		try:
			m = data['VMManage']['VM'][vmname]
		except Exception as e:
			yield "Error: VM is not manageable by this bot"
			return

		# check if RM or classic -> different webhook
		try:
			deployType = data['VMManage']['VM'][vmname]['deploymentType']
			if (deployType == "RM"):
				resGroup = data['VMManage']['VM'][vmname]['resourcegroup']
				body = {'vmname': vmname, 'resourcegroup': resGroup, 'action': action}
				uri = data['VMManage']['Webhook-RM']
				yield "Sending request to Start " + vmname + "..."
				r = self.post_webhook(uri)
				yield r
			elif (deployType == "Classic"):
				body = {'vmname': vmname, 'action': action}
				uri = data['VMManage']['Webhook-Classic']
				yield "Sending request to Start " + vmname + "..."
				r = self.post_webhook(uri)
				yield r
		except Exception as e:
			yield "Exception in vmmanage : " + str(e)
