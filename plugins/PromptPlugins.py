from errbot import BotPlugin, botcmd, arg_botcmd, cmdfilter
# from errbot.rendering import imtext md_converter = imtext() # you need to cache the converter

# TODO: put configuration in an configuration file.
# VictorOps
vo_defaults_team = ('infrastructure', 'application')
vo_api_id = '0ce1b0c2'
vo_api_key = '3d8722d701ce4ed199512fb773a39940'

# Twilio
tw_account_sid = "ACcaaecc7c2fc2726cac8a9b021c5cd5dc" # Your Account SID from www.twilio.com/console
tw_auth_token = "66ac22785485bc4766e8931a056b8e52"  # Your Auth Token from www.twilio.com/console

class PromptPlugins(BotPlugin):

    def get_oncall_json(self, team):
        from requests import get
        r = get(
            u"https://api.victorops.com/api-public/v1/team/{}/oncall/schedule?daysForward=0&daysSkip=0&step=0".format(
                team),
            headers={'Accept': 'application/json',
                     'X-VO-Api-Id': vo_api_id, 'X-VO-Api-Key': vo_api_key
                     }
        )

        return r.text

    @botcmd
    @arg_botcmd('--t', dest='team', type=str)
    def checkoncall(self, msg, team):
        """
         This plugin will allow you to check the current OnCall operators for a team X.
        """
        from json import loads
        # #from pprint import pformat
        message = '!checkoncall:\n'
        if team is None:
            teams = vo_defaults_team
        else:
            teams = tuple(team.split(','))
        for t in teams:
            jsteam=loads(self.get_oncall_json(t))
            # #message += "`\n"+pformat(jsteam) + "\n`\n"
            message += "Team: *"+jsteam['team'] + "*\n"
            for s in jsteam['schedule']:
                message += "> - Scheduled on call: @ *"+s['onCall'] + "*\n"
            for s in jsteam['overrides']:
                message += "> - Override by: @ *"+s['overrideOnCall'] + "*\n"
            message +="\n \n \n"
        return message



    @botcmd
    @arg_botcmd('--d', dest='direction', type=str)
    @arg_botcmd('--n', dest='phoneNumber', type=int)
    @arg_botcmd('--c', dest='count', type=int, default=3)
    def checktwilio(self, msg, direction=None, phoneNumber=None, count=None):
        """
        This plugin will allow you to check the current OnCall operators for a team X.
        """
        from twilio.rest import TwilioRestClient
        isError = False
        errorMessage = ''
        messageList = ''
        messages = "checktwilio:\n"

        def SentTo(number):
            """Return All messages sent to """
            client = TwilioRestClient(tw_account_sid, tw_auth_token)
            messages = client.messages.list(
                to=number,
            )
            return messages

        def SentFrom(number):
            """Return All messages sent from "number" """
            client = TwilioRestClient(tw_account_sid, tw_auth_token)
            messages = client.messages.list(
                from_=number,
            )
            return messages

        if direction == 'To':
            messages = SentTo("+1" + str(phoneNumber))
        elif direction == 'From':
            messages = SentFrom("+1" + str(phoneNumber))
        else:
            isError = True
            errorMessage = '**{0}** is an invalid direction. Please use \'To\' or \'From\'.'.format(direction)
        for message in messages[:int(count)]:
            messageList += ' **Message ID:** {0} \n **Sent at:** {1} \n **To:** {2} \n' \
                           ' **From:** {3} \n **Status:** {4}&nbsp;&nbsp;\n&nbsp;&nbsp;\n'\
                .format(str(message.sid), str(message.date_sent),
                        str(message.to.replace("+1", "")),
                        str(message.from_.replace("+1", "")), message.status)
        if isError == True:
            returnValue = errorMessage
        else:
            returnValue = '##Here are the last {0} entries sent {1} {2}:\n {3}'\
                .format(str(count), direction, str( phoneNumber ), messageList)
        return returnValue

    @botcmd
    def whoiam(self, msg, args):
        """
        Who I am: List membership also
        """
        from helpers import is_channel_member
        # #from pprint import pformat
        backend=self._bot
        # #slackclient=backend.sc
        message = "!whoiam:\n"
        sender=msg.frm
        message += "#Errbot\n> - " + sender.fullname + " ( " + sender.client + " ) @" + sender.nick + "\n"
        if hasattr(sender, 'room'):
            message += "\n> room is " + sender.room + "\n"
        message += "#Slack#\n> - @"+sender.username+" ( "+sender.userid+" ) \n"
        message += "## Member of channels:\n"
        for c in backend.channels():
            if sender.userid in c['members']:
                message += ">>> `#" + c['name'] + "` \r\n"
            # #message+=c['name']+"\n"
        message+="\n\n"
        if is_channel_member(self, sender.userid, 'pa_oncall'):
             message += "##You are an onCall members\n"
        else:
             message += "##Not member of onCall\n"
        if '@'+sender.nick in self.bot_config.BOT_ADMINS:
            message += "##You are a Errbot Admin"
        else:
            message += "##Not a Errbot Admin"
        return message
